﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentRedirect.aspx.cs"
    Inherits="teamdk.Backend.Utilities.PaymentRedirect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Secure payment redirect</title>
    <style type="text/css">
      body {
	margin:50px 0px; padding:0px;
	text-align:center;
	}
	
#content {
	width:500px;
	margin:0px auto;
	text-align:center;
	padding:15px;
	border:3px dashed #333;
	background-color:#eee;
	}

    </style>
</head>
<body>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           $("#<%=btnPayPal.ClientID %>").click();
        });
    </script>

    <form id="myform" runat="server" >
<div id="content">
	<h1>
        Omdirigerer til sikker betaling</h1>
    <br />
        <img src="/imagegen.ashx?image=/image/bodyguard-cartoon-character.png&height=300" />
    <br />
    <br />
    If you are not redirected automaticaly after 10 seconds click here: <asp:Button ID="btnPayPal" runat="server" Text="Proceed" />
   		
</div>
     <input type="hidden" name="cmd" value="_s-xclick"/>
    <asp:Literal runat="server" ID="strEncrypt" />
    
    </form>
    
    
    <%--1 month subscription--%>
    <asp:PlaceHolder ID="OneMonthSubsciprtionPlaceHolder" runat="server" Visible="false">
        <%--Skarp--%>
        <%--    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="myform">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="YXNFA8F488BBJ">
        <input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_buynow_LG.gif" border="0"
            name="submit" alt="PayPal - The safer, easier way to pay online.">
        <img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1"
            height="1">
        </form>--%>
        <%--test--%>
        <%--   <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="myform" >
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="2DUYUHHMAGA4Q">
<input type="image" src="https://www.sandbox.paypal.com/en_GB/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>
        --%>
    </asp:PlaceHolder>
    <%--<form action="https://secure.quickpay.dk/quickpay.php" id="myform" name="myform" method="post">
						<input type="hidden" name="language" value="{$Language}" />
						<input type="hidden" name="autocapture" value="{$AutoCapture}" />
						<input type="hidden" name="ordernum" value="{format-number($OrderID, '0000')}" />
						<input type="hidden" name="merchant" value="{$Merchant}" />
						<input type="hidden" name="amount" value="{$Amount}" />
						<input type="hidden" name="currency" value="{$Currency}" />
						<input type="hidden" name="okpage" value="{$ContinueUrl}" />
						<input type="hidden" name="errorpage" value="{$CancelUrl}" />
						<input type="hidden" name="resultpage" value="{$CallbackUrl}" />
						<input type="hidden" name="md5checkV2" value="{Shop:MD5(concat($Language, $AutoCapture, format-number($OrderID, '0000'), $Amount, $Currency, $Merchant, $ContinueUrl, $CancelUrl, $CallbackUrl, $ccipage, $SecretMd5 ))}" />
						<input type="hidden" name="ccipage" value="{$ccipage}" />
						<input type="submit" name="submit2" value="Videre til betaling" />
					</form>
		--%>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'XXX']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
