﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MailChimpSubscibeSimple.ascx.cs" Inherits="MailChimp4Umbraco.Userinterface.MailChimp4Umbraco.MailChimpSubscibeSimple" %>
<div class="newsletter newsletter-subscribe">
    <asp:Literal ID="MessageLiteral" runat="server"></asp:Literal>
    <div class="newsletter-email">
        <asp:Label ID="NewsletterEmailLabel" Text="" AssociatedControlID="NewsletterEmailInput" runat="server"></asp:Label>
        <asp:TextBox ID="NewsletterEmailInput" runat="server"></asp:TextBox>
    </div>
    <div class="newsletter-submit">
        <asp:Button ID="NewsletterSubmit" runat="server" Text="Subscribe" onclick="Button1_Click" />
    </div>
</div>
