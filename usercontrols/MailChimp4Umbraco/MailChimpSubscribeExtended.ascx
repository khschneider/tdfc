﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MailChimpSubscribeExtended.ascx.cs" Inherits="MailChimp4Umbraco.Userinterface.MailChimp4Umbraco.MailChimpSubscribeExtended" %>
<div class="newsletter newsletter-subscribe">
	<asp:Literal ID="MessageLiteral" runat="server"></asp:Literal>
    <div class="newsletter-email">
        <asp:Label ID="EmailAddressLabel" Text="" AssociatedControlID="NewsletterEmailInput" runat="server"></asp:Label>
        <asp:TextBox ID="NewsletterEmailInput" runat="server"></asp:TextBox>
    </div>
    <div class="newsletter-name">
        <asp:Label AssociatedControlID="NewsletterNameInput" ID="NameLabel" runat="server" Text=""></asp:Label>
        <asp:TextBox ID="NewsletterNameInput" runat="server"></asp:TextBox>
    </div>
    <div class="newsletter-lastname">
        <asp:Label ID="LastNameLabel" AssociatedControlID="NewsletterLastNameInput" runat="server" Text=""></asp:Label>
        <asp:TextBox ID="NewsletterLastNameInput" runat="server"></asp:TextBox>
    </div>
    <div class="newsletter-submit">
        <asp:Button ID="NewsletterSubmit" runat="server" Text="Subscribe" onclick="Button1_Click" />
    </div>
</div>