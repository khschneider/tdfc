﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MailChimpUnSubscribe.ascx.cs" Inherits="MailChimp4Umbraco.Userinterface.MailChimp4Umbraco.MailChimpUnSubscribe" %>
<div class="newsletter newsletter-unsubscribe">
	<asp:Literal ID="MessageLiteral" runat="server"></asp:Literal>
    <div class="newsletter-email">
        <asp:Label ID="NewsletterEmailLabel" Text="" AssociatedControlID="NewsletterEmailInput" runat="server"></asp:Label>
        <asp:TextBox ID="NewsletterEmailInput" runat="server"></asp:TextBox>
    </div>
    <div class="newsletter-submit">
        <asp:Button ID="NewsletterSubmit" runat="server" Text="UnSubscribe" onclick="Button1_Click" />
    </div>
</div>