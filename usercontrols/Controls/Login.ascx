﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="teamdk.usercontrols.Controls.Login" %>
<asp:LoginView ID="Loginview1" runat="server">
    <AnonymousTemplate>
        <a id="fbLogin" href="#" onclick="loginUser(); return false;">
            <img src="/imagegen.ashx?image=/media/images/facebooklogin.png&height=60" /></a>
       <%-- <br/>
        eller
        <br/>
        <br/>
        <div style="margin: 0 auto;width: 280px;font-size: 90%;">
        <asp:Login ID="Login1" runat="server">
            <LayoutTemplate>
                <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                    <tr>
                        <td>
                            <table cellpadding="0">
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Brugernavn:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Brugernavn er nødvendigt" ToolTip="Brugernavn er nødvendigt" ValidationGroup="ctl00$ctl01$Login1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password er nødvendigt" ToolTip="Password er nødvendigt." ValidationGroup="ctl00$ctl01$Login1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                             
                                <tr>
                                    <td align="center" colspan="2" style="color:Red;">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <br/>
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="ctl00$ctl01$Login1" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
        </asp:Login>
        </div>--%>
    </AnonymousTemplate>
    <LoggedInTemplate>
        Du er lige nu logget ind.
        <asp:LoginStatus ID="LoginStatus1" runat="server" OnLoggedOut="HandleLogout" />
    </LoggedInTemplate>
</asp:LoginView>
