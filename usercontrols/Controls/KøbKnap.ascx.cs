﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using teamdk.Backend.DomainModel;
using teamdk.Backend.Utilities;
using umbraco.cms.businesslogic.member;

namespace teamdk.usercontrols.Controls
{
    public partial class KøbKnap : System.Web.UI.UserControl
    {
        public int Price { get; set; }
        public string ItemName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached) return;
            
            if (!IsPostBack)
            {

                if (Member.GetCurrentMember() == null)
                {
                    Button1.Visible = false;
                    label1.Visible = true;
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var currentPayment = new PaymentContainer();
            currentPayment.isItemPayment = true;
            if (Member.GetCurrentMember() != null)
            {
                currentPayment.medlem = new Medlem(Member.GetCurrentMember());

            }
            if (System.Diagnostics.Debugger.IsAttached)
            {
                currentPayment.amount = 0.01;
            }
            else
            {
                currentPayment.amount = Price;
            }
            currentPayment.ReturnUrl = "/tak-for-dit-koeb/";
            currentPayment.ItemName = ItemName;
            SessionWrapper.CurrentPayment = currentPayment;
            Response.Redirect("/PaymentRedirect.aspx");
        }
    }
}