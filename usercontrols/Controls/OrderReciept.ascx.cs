﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using teamdk.Backend;
using teamdk.Backend.DomainModel;
using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;
using teamdk.Backend.Utilities;
using teamdk.Backend.Utilities.paypal.util;
using umbraco.cms.businesslogic.member;
using umbraco.Linq.Core;

namespace teamdk.usercontrols.Controls
{
    public partial class OrderReciept : System.Web.UI.UserControl
    {

        private const string AMPERSAND = "&";
        private const string EQUALS = "=";
        private static readonly char[] AMPERSAND_CHAR_ARRAY = AMPERSAND.ToCharArray();
        private static readonly char[] EQUALS_CHAR_ARRAY = EQUALS.ToCharArray();

        public String OrderId { get; set; }
        public String OrderTotal { get; set; }
        public String ItemName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {



            var mem = new Medlem(Member.GetCurrentMember());
            if (Request.QueryString.AllKeys.Contains("cancel"))
            {
                MultiView1.ActiveViewIndex = 1;
                if (Member.GetCurrentMember() != null)
                {
                    nameLabel.Text = mem.FirstName;
                }

                errorLabel.Text = "Du afbrød dit køb";
                return;
            }
            try
            {
                Email.SendPaymentMail(mem);

            }
            catch (Exception ex)
            {
                Minilogger.Error(ex);
            }

            if (!IsPostBack)
            {
                using (var ctx = new Entities())
                {
                    //read the respons from paypal
                    var responsHash = new Dictionary<string, string>();
                    string incomingIpn;
                    using (var reader = new StreamReader(Request.InputStream))
                    {

                        incomingIpn = reader.ReadToEnd();
                        foreach (string nvp in incomingIpn.Split(AMPERSAND_CHAR_ARRAY))
                        {
                            string[] tokens = nvp.Split(EQUALS_CHAR_ARRAY);
                            if (tokens.Length >= 2)
                            {
                                string name = HttpUtility.UrlDecode(tokens[0]);
                                string valuepair = HttpUtility.UrlDecode(tokens[1]);
                                responsHash.Add(name, valuepair);
                            }
                        }
                    }
                    Minilogger.Debug("incomingIpn: " + incomingIpn);
                    Order order;


                    if (responsHash.ContainsKey("custom"))
                    {
                        var guid = Guid.Parse(responsHash["transaction_subject"]);
                        order = ctx.Orders.SingleOrDefault(o => o.Guid == guid);
                    }
                    else
                    {
                        var guid = Guid.Parse(Request.QueryString["cm"]);
                        order = ctx.Orders.SingleOrDefault(o => o.Guid == guid);

                    }
                    if (order == null)
                    {
                        //This is not the same session!
                        MultiView1.ActiveViewIndex = 1;
                        errorLabel.Text =
                            "This is not the same session that we started with. I cannot continue, please inform support about sessionID: " +
                            Session.SessionID;
                        if (responsHash.ContainsKey("transaction_subject"))
                        {
                            Minilogger.Error("transaction_subject is " + responsHash["transaction_subject"]);
                        }
                        Minilogger.Error("session id is: " + Session.SessionID);
                        return;
                    }
                
                    order.PaypalResponse = Request.Url.Query;

                    //did the payment go ok? check with paypal
                    if (incomingIpn.Length != 0)
                    {
                        var ipnPostback = incomingIpn + "&cmd=_notify-validate";
                        var ipnResponse = Utils.HttpPost("https://www.paypal.com/cgi-bin/webscr", ipnPostback);
                        order.PaypalResponse = ipnResponse;

                        Minilogger.Debug("ipnResponse " + ipnResponse);

                        if (ipnResponse != "VERIFIED")
                        {
                            MultiView1.ActiveViewIndex = 1;
                            errorLabel.Text = "The payment was not verifed by paypal, they said" + ipnResponse;
                            Minilogger.Error("The payment was not verifed by paypal, they said" + ipnResponse);
                            return;
                        }
                    }
                    else
                    {
                        if (Request.QueryString["st"] != "Completed")
                        {
                            MultiView1.ActiveViewIndex = 1;
                            order.PaypalResponse = Request.QueryString["st"];

                            errorLabel.Text = "The payment was not verifed by paypal, they said" +
                                              Request.QueryString["st"];
                            Minilogger.Error("The payment was not verifed by paypal, they said" +
                                             Request.QueryString["st"]);

                            return;
                        }
                        else
                        {
                            order.PaypalResponse = "Completed";
                        }
                      
                    }
                    order.PayEnd = DateTime.Now;
                    ctx.SaveChanges();

                    //set google stuff:
                    try
                    {
                        ItemName = SessionWrapper.CurrentPayment.ItemName;
                        OrderId = Request.QueryString["tx"];
                        OrderTotal = Request.QueryString["amt"];

                    }
                    catch (Exception ex)
                    {
                        Minilogger.Error("error setting google stuff: " + ex.ToString());
                    }


                    //everything went fine
                    try
                    {
                        MultiView1.ActiveViewIndex = 0;
                        try
                        {
                            nameLabel.Text = mem.FirstName;
                            errornameLabel.Text = mem.FirstName;
                        }
                        catch (Exception)
                        {
                        }
                        if (order.isQuotaPayment)
                        {
                            mem.BetalingsTidspunkt = DateTime.Now;

                            if (order.isHalfQuota)
                            {
                                mem.MedlemsType = MedlemsTypeEnum.Halvårs;
                            }
                            else
                            {
                                mem.MedlemsType = MedlemsTypeEnum.Helårs;
                            }
                        }
                        else if (order.isItemPayment)
                        {
                            nameLabel.Text = "Tak for dit køb";
                        }
                        else
                        {
                            var @event =TeamDkDataContext.Instance.Events.Single(evt=>evt.Id==order.eventId);
                            @event.InsertDeltager(mem.Id.ToString());
                        }
                        Minilogger.Debug("Done setting subscription on medlem");
                        Response.Redirect(order.ReturnUrl);

                    }
                    catch (Exception ex)
                    {
                        Minilogger.Error(ex);
                        MultiView1.ActiveViewIndex = 1;
                        errorLabel.Text = ex.ToString();
                        return;
                    }
                    this.DataBind();
                }
            }

        }
    }
}