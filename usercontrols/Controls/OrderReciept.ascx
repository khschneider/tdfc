﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderReciept.ascx.cs" Inherits="teamdk.usercontrols.Controls.OrderReciept" %>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div id="recieptleft">
                Tak for det.
                <asp:Label ID="nameLabel" runat="server" Text=""></asp:Label>
                <umbraco:Item ID="Item1" runat="server" Field="Content" />
              
            </div>
            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-17668559-16']);
                _gaq.push(['_trackPageview']);
                _gaq.push(['_addTrans',
                            '<%# this.OrderId %>',           // order ID - required
                            null,  // affiliation or store name
                            '<%# this.OrderTotal %>',          // total - required
                            null,           // tax
                            null,              // shipping
                            null,       // city
                            null,     // state or province
                            null             // country
                          ]);

                // add item might be called for every item in the shopping cart
                // where your ecommerce engine loops through each item in the cart and
                // prints out _addItem for each
                _gaq.push(['_addItem',
                        '<%# this.OrderId %>',           // order ID - required
                        '<%# this.ItemName %>',  // SKU/code - required
                        '<%# this.ItemName %>',          // product name
                        null,                           // category or variation
                        '<%# this.OrderTotal %>',       // unit price - required
                        '1'                             // quantity - required
                      ]);
                _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

                (function () {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();

            </script>
        </asp:View>
        <asp:View ID="View2" runat="server">
            Sorry
            <asp:Label ID="errornameLabel" runat="server" Text="."></asp:Label>&nbsp The payment
            experienced an error.
            <br />
            <br />
            This is what the system told us:
            <asp:Label ID="errorLabel" runat="server" Text="Error message" ForeColor="Red"></asp:Label>
            <br />
        </asp:View>
    </asp:MultiView>
