﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;

namespace teamdk.usercontrols.Controls
{
    public partial class Login : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void HandleLogout(object sender, EventArgs e)
        {
            var fb = new FacebookClient();
            dynamic result = fb.Get("oauth/access_token", new
            {
                client_id = "403663173025186",
                client_secret = "0e878a0c210bb7355fb1306a8d3d7881",
                grant_type = "client_credentials"
            });

            var logout = fb.GetLogoutUrl(new { access_token = result.access_token, next = "http://" + HttpContext.Current.Request.Url.Authority
            }).ToString();
            Response.Redirect(logout);
        }
    }
}