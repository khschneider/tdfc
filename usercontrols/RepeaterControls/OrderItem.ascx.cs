﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using teamdk.Backend.DomainModel;
using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;
using umbraco.cms.businesslogic.member;

namespace teamdk.usercontrols.RepeaterControls
{
    public partial class OrderItem : System.Web.UI.UserControl
    {
        public Order Order { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string EventRender(int? eventId)
        {
            if (eventId == null) return "-";
            try
            {
                    var evt = TeamDkDataContext.Instance.Events.Single(e => e.Id == eventId);
                    return evt.NodeName;
           
            }
            catch (Exception)
            {

                return "?";
            }
            
        }

        protected string MemberRender(int memberId)
        {
            var mem = new Medlem(new Member(memberId));
            return mem.FullName;
        }
    }
}