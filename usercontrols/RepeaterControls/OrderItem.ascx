﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderItem.ascx.cs" Inherits="teamdk.usercontrols.RepeaterControls.OrderItem" %>
<tr>
    <td><%#Order.OrderId %></td>
    <td><asp:CheckBox runat="server" Enabled="False" Checked="<%#Order.isFullQuota %>"/></td>
    <td><asp:CheckBox  runat="server" Enabled="False" Checked="<%#Order.isHalfQuota %>"/></td>
    <td><%#EventRender(Order.eventId) %></td>
    <td><%#MemberRender(Order.memberId) %></td>
    <td><%#Order.amount %></td>
    <td><%#Order.buingItemName %></td>
    <td><%#Order.PaypalResponse %></td>
    <td><%#Order.PayStart %></td>
    <td><%#Order.PayEnd %></td>
</tr>
