﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Oversigt.ascx.cs" Inherits="teamdk.usercontrols.Dashboard.Oversigt" %>
<%@ Import Namespace="teamdk.Backend.DomainModel" %>
<%@ Register Src="~/usercontrols/RepeaterControls/OrderItem.ascx" TagPrefix="uc1" TagName="OrderItem" %>
<style>
table,th,td
{
    border:1px solid #333;
    border-collapse: collapse;
    padding: 3px;
    
}
</style>
<h1>Paypal Transaktioner</h1>
<table>
    <thead>
        <tr>
        <th>Order Id</th>
        <th>Købt helårs</th>
        <th>Købt halvårs</th>
        <th>Event</th>
        <th>Medlem</th>
        <th>Beløb</th>
        <th>Vare</th>
        <th>PP Svar</th>
        <th>Køb startet</th>
        <th>Køb slutet</th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater runat="server" ID="repeater">
            <ItemTemplate>
                <uc1:OrderItem runat="server" id="OrderItem" Order=<%#(Order)Container.DataItem %> />
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
</table>
