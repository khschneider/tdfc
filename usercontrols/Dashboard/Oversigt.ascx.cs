﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using teamdk.Backend.DomainModel;

namespace teamdk.usercontrols.Dashboard
{
    public partial class Oversigt : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var ctx = new Entities())
                {
                    repeater.DataSource = ctx.Orders.ToList().OrderByDescending(o=>o.OrderId);
                }
                repeater.DataBind();
            }
        }
    }
}