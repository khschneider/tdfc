﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using teamdk.Backend.DomainModel;

namespace teamdk.usercontrols.Datatypes
{
    public partial class EventInfo : System.Web.UI.UserControl, umbraco.editorControls.userControlGrapper.IUsercontrolDataEditor 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (UmbracoValue != "")
                {
                    SetFieldsFromSerializedObject();
                }
                else
                {
                    BetalingRBList.SelectedIndex = 0;
                    BetalingsTypeRBList.SelectedIndex = 0;
                }
            }
            else
            {
                UmbracoValue = GetSerializedObject();
            }
        }

        private void SetFieldsFromSerializedObject()
        {
            var obj = EventBetaling.DeSerialize(UmbracoValue);

            if (obj.Gratis)
            {
                BetalingRBList.SelectedIndex = 0;
            }
            else
            {
                BetalingRBList.SelectedIndex = 1;
                prisTextBox.Text = obj.Pris.ToString();
                if (obj.DiffBetaling)
                {
                    BetalingsTypeRBList.SelectedIndex = 1;
                    diffPrisTextBox.Text = obj.DiffPris.ToString();
                }
                else
                {
                    BetalingsTypeRBList.SelectedIndex = 0;
                }
            }
        }

        private string GetSerializedObject()
        {
            var et = new EventBetaling();
            if(BetalingRBList.SelectedValue=="Gratis")
            {
                et.Gratis = true;
            }
            else
            {
                if (BetalingsTypeRBList.SelectedValue == "Forskellig pris")
                {
                    et.DiffBetaling = true;
                    et.DiffPris = int.Parse(diffPrisTextBox.Text);
                }
                et.Pris = int.Parse(prisTextBox.Text);
            }
            return et.Serialize();
        }

        public string UmbracoValue;

        public object value
        {
            get
            {
                return UmbracoValue;
            }
            set
            {
                UmbracoValue = value.ToString();
            }
        }
    }
}