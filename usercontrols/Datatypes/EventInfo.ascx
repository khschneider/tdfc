﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventInfo.ascx.cs" Inherits="teamdk.usercontrols.Datatypes.EventInfo" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("input[name='<%=BetalingRBList.UniqueID%>']").click(function () {
            if ($(this).val() == "Gratis") {
                $('#betaling').hide('slow');
            } else {
                $('#betaling').show('slow');
            }
        });
        $("input[name='<%=BetalingsTypeRBList.UniqueID%>']").click(function () {
            $('#PrisDiv').show('slow');
            if ($(this).val() != "Forskellig pris") {
                $('#DiffPris').hide('slow');
            } else {
                $('#DiffPris').show('slow');
            }
        });
        if ($('#<%=BetalingRBList.ClientID%> input:checked').val() != 'Gratis') {
            $('#betaling').show();
        }
        if ($('#<%=BetalingsTypeRBList.ClientID%> input:checked').val() != '') {
            $('#PrisDiv').show();
        }
        if ($('#<%=BetalingsTypeRBList.ClientID%> input:checked').val() == 'Forskellig pris') {
            $('#DiffPris').show();
        }
    });
</script>
<asp:RadioButtonList ID="BetalingRBList" runat="server">
    <asp:ListItem>Gratis</asp:ListItem>
    <asp:ListItem>Betaling</asp:ListItem>
</asp:RadioButtonList>
<div id="betaling" style="display: none; margin-left: 20px;">
    <asp:RadioButtonList ID="BetalingsTypeRBList" runat="server" >
        <asp:ListItem>Samme pris for medlemmer og ikke medlemmer</asp:ListItem>
        <asp:ListItem>Forskellig pris</asp:ListItem>
    </asp:RadioButtonList>
    <div id="PrisDiv" style="padding-top: 10px; display: none; margin-left: 10px;">
        Pris:<br />
        £<asp:TextBox runat="server" ID="prisTextBox"></asp:TextBox>
        <div id="DiffPris" style="padding-top: 10px; display: none;">
            Pris for ikke medlemmer:<br/>
            £<asp:TextBox ID="diffPrisTextBox" runat="server" prisTextBox></asp:TextBox>
        </div>
    </div>
</div>
