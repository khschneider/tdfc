﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

using teamdk.Backend.DomainModel;
using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;
using teamdk.usercontrols.Controls;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.NodeFactory;

namespace teamdk.Backend
{
    public class Email
    {

        public static void SendOldMemberMail(Medlem medlem)
        {
            try
            {


                var settingsId = TeamDkDataContext.Instance.Settingss.Single().Id;
                var n = new Node(settingsId);
                var emails = "";
                var message = new System.Net.Mail.MailMessage();
                if (n.HasProperty("emailNotifikationer"))
                {
                    emails = n.GetProperty("emailNotifikationer").Value;
                }
                if (emails.Length > 0)
                {
                    foreach (var email in emails.Split(','))
                    {
                        if (string.IsNullOrEmpty(email)) continue;
                        message.To.Add(email);
                    }

                    message.Subject = "Tidligere medlem";
                    message.From = new System.Net.Mail.MailAddress("teamdenmarkfc@gmail.com");
                    message.Body = "Følgende har markeret at vedkommende er tidligere betalende medlem: " + medlem.FullName + " " + medlem.email;
                    SendMail(message);
                }
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, ex.ToString());
            }
        }

        public static void SendPaymentMail(Medlem medlem)
        {
            try
            {


                var settingsId = TeamDkDataContext.Instance.Settingss.Single().Id;
                var n = new Node(settingsId);
                var emails = "";
                var message = new System.Net.Mail.MailMessage();
                if (n.HasProperty("emailNotifikationer"))
                {
                    emails = n.GetProperty("emailNotifikationer").Value;
                }
                if (emails.Length > 0)
                {
                    foreach (var email in emails.Split(','))
                    {
                        if (string.IsNullOrEmpty(email)) continue;
                        message.To.Add(email);
                    }

                    message.Subject = "Betaling medlem";
                    message.From = new System.Net.Mail.MailAddress("teamdenmarkfc@gmail.com");
                    message.Body = "Ny betaling har fundet sted for " + medlem.FullName + " " + medlem.email; ;
                    SendMail(message);
                }
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, ex.ToString());
            }
        }

        private static void SendMail(MailMessage message)
        {
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                Minilogger.Debug(ex.ToString());

            }

        }
    }
}