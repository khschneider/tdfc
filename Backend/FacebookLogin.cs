﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Facebook;
using System.Web;
using teamdk.Backend.DomainModel;
using uComponents.Core;
using umbraco.BusinessLogic;
using umbraco.MacroEngines.Library;
using umbraco.cms.businesslogic.relation;
using System.Text.RegularExpressions;
using umbraco.presentation.nodeFactory;
using umbraco.cms.businesslogic.member;
using umbraco.cms.businesslogic.propertytype;
using umbraco.presentation.umbracobase;

namespace teamdk.Backend
{
    [RestExtension("FacebookLogin")]
    public class FacebookLogin
    {

        [RestExtensionMethod()]
        public static string LogUserIn()
        {
            string accessToken = HttpContext.Current.Request["accessToken"];
            try
            {
                var client = new FacebookClient(accessToken);
                dynamic user = client.Get("me");
                /*we have authenticated the user now we must either create a new member or log them is as an existing member*/
                List<Member> existingMember = uComponents.Core.uQuery.GetMembersByXPath("//*[facebookUserId = '" + user.id + "']");
                if (existingMember.Any())
                {
                    Member.AddMemberToCache(existingMember[0]);
                    return existingMember[0].Id.ToString();
                }
                else
                {
                    return createNewUser(user);
                }
            }
            catch (Exception e)
            {
                return e.Message + " " + e.StackTrace;
            }
        }
        

        private static String createNewUser(dynamic facebookUser)
        {
            MemberType siteUser = new MemberType(1054);
            Member newMember = Member.MakeNew(facebookUser.name, siteUser, new User(0));

            newMember.getProperty("facebookUserId").Value = facebookUser.id;
            newMember.getProperty("firstname").Value = facebookUser.first_name;
            newMember.getProperty("lastname").Value  = facebookUser.last_name;
            newMember.getProperty("birthday").Value = facebookUser.birthday;
            newMember.LoginName = facebookUser.name;
            newMember.Email = facebookUser.email;
            Roles.AddUserToRole(facebookUser.name,"Medlem");
            newMember.Save();
            Member.AddMemberToCache(newMember);
            var mem = new Medlem(newMember);
            Log.Add(LogTypes.Debug, 0,"Setting mailchimpsubscription for "+facebookUser.email);
            mem.TilmeldNyhedsBrev("Alle", HttpContext.Current.Request.UserHostAddress);
            mem.TilmeldNyhedsBrev("Fodbold", HttpContext.Current.Request.UserHostAddress);
            mem.TilmeldNyhedsBrev("Løb", HttpContext.Current.Request.UserHostAddress);
            mem.TilmeldNyhedsBrev("Golf", HttpContext.Current.Request.UserHostAddress);
            mem.TilmeldNyhedsBrev("Social", HttpContext.Current.Request.UserHostAddress);

            return newMember.Id.ToString();
        }
    }
}