﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using ImageResizer;
using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;
using teamdk.Backend.Utilities;
using umbraco.BusinessLogic;
using umbraco.businesslogic;
using umbraco.cms.businesslogic;
using umbraco.cms.businesslogic.media;
using umbraco.cms.businesslogic.web;

namespace teamdk.Backend
{
    public class UmbracoEventHandler : ApplicationStartupHandler
    {
        public UmbracoEventHandler()
        {
            Document.AfterPublish += new Document.PublishEventHandler(Document_BeforePublish);
            Media.BeforeSave += new Media.SaveEventHandler(Media_BeforeSave);
            Media.BeforeMoveToTrash += new Media.MoveToTrashEventHandler(Media_BeforeDelete);
        }

        private void Media_BeforeDelete(Media sender, MoveToTrashEventArgs moveToTrashEventArgs)
        {
            if(sender.ContentType.Alias=="Folder") return;
                var path = (string) sender.getProperty("umbracoFile").Value;
                AzureWrapper.DeleteBlob(path);
        }

        private void Media_BeforeSave(Media sender, SaveEventArgs e)
        {
            if (sender.ContentType.Alias == "Folder") return;
            if(sender.Path.Contains("1119"))//1119 =gallery
            {
               // ThreadPool.QueueUserWorkItem(new WaitCallback(UploadToAzure),sender);
                UploadToAzure(sender);
            }
            umbraco.macro.GetMacro(9).removeFromCache();
        }

        private static void UploadToAzure(object sender)
        {
            try
            {

         
            var m = (Media) sender;
            var appPath = HttpRuntime.AppDomainAppPath;
            var path = (string)m.getProperty("umbracoFile").Value;
            var loc = appPath + path;
            var fi = new FileInfo(loc);
            var thumbName = fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length) + "_blobthumb" + fi.Extension;
            var thumbPath = fi.Directory +"\\" + thumbName;
            ImageBuilder.Current.Build(loc, thumbPath, new ResizeSettings() {Width = 430,Mode = FitMode.Max});
            ImageBuilder.Current.Build(loc, loc, new ResizeSettings() { Width = 1440, Height = 1440, Mode = FitMode.Max });
            
            var url = AzureWrapper.UploadToBlob(path);
            var thumburl = AzureWrapper.UploadToBlob(thumbPath.Replace(appPath,""));

            var fiThumb = new FileInfo(thumbPath);
            fiThumb.Delete();

            m.getProperty("blobUrl").Value = url;
            m.getProperty("blobThumbUrl").Value = thumburl;


            ImageBuilder.Current.Build(loc, loc, new ResizeSettings(100, 100, FitMode.Pad, null));
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Error, 0,ex.ToString());
            }
        }

        private void Document_BeforePublish(Document sender, PublishEventArgs e)
        {
            if (sender.ContentType.Alias == "Event")
            {
                var aktivitet = sender.getProperty("aktivitet").Value;
                var evt =
                    TeamDkDataContext.Instance.Events.SingleOrDefault(
                        ev => ev.Id == sender.Id);
               // MailChimpApiWrapper.SendEventCreation(evt);
            }
        }
    }
}