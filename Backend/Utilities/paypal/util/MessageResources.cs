/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Threading;

namespace teamdk.Backend.Utilities.paypal.util
{
	/// <summary>
	/// Utility class used for internationalization
	/// </summary>
	public abstract class MessageResources
	{
		/// <summary>
		/// Resource bundle
		/// </summary>
        protected static ResourceManager ResourceBundle = new ResourceManager("com.paypal.wps.util.Resource", Assembly.GetExecutingAssembly());		


		/// <summary>
		/// Get a localized message
		/// </summary>
		/// <param name="message">key to reference in resource bundle</param>
		/// <returns>localized string</returns>
		public static string GetMessage(string message)
		{
			return ResourceBundle.GetString(message);
		} // GetMessage


		/// <summary>
		/// Set the locale
		/// </summary>
		/// <param name="language">language, eg. "EN"</param>
		/// <param name="country">country, eg. "US"</param>
		public static void SetLocale(string language, string country)
		{
			string culture = language+"-"+country;
			Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
		} // SetLocale
	} // MessageResources class
} // util namespace