﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using com.paypal.wps.exceptions;
using com.paypal.wps.rules;
using teamdk.Backend.Utilities.paypal.profiles;
using teamdk.Backend.Utilities.paypal.util;
using teamdk.Backend.Utilities.paypal.util;

namespace teamdk.Backend.Utilities.paypal
{
    public class EWPServices
    {
        /// <summary>
        /// Active profile
        /// </summary>
        private IEWPProfile profile;

        #region Properties
        /// <summary>
        /// Get/Set the EWP profile.
        /// </summary>
        public IEWPProfile EWPProfile
        {
            get
            {
                return this.profile;
            }
            set
            {
                ValidationErrors errors = value.Validate();
                if (!errors.IsEmpty())
                {
                    StringBuilder msg = new StringBuilder(MessageResources.GetMessage("PROFILE_INVALID"));
                    IEnumerator iEnum = errors.GetEnumerator();
                    while (iEnum.MoveNext())
                    {
                        ValidationError error = (ValidationError)iEnum.Current;
                        msg.Append("\n" + error.Key);
                    }
                    throw new TransactionException(msg.ToString());
                }
                this.profile = value;
            }
        }
        #endregion

        #region public Methods
        /// <summary>
        /// Encrypt button data and Write the encrypted data 
        /// and button code in to string.
        /// </summary>
        /// <param name="buttonParams">key=value, '\n' separated list 
        /// of button parameters</param>
        /*    public string EncryptButton(string buttonParams, string itemName, string itemAmount)
            {
                // Make sure a profile has been set
                if (profile == null)
                {
                    throw new WarningException(MessageResources.GetMessage("NO_PROFILE_SET"));
                }
                // Get the encrypted button data
                string encrypted;
                try
                {

                    for (int i = 0; i < 255; i++)
                    {
                        buttonParams = buttonParams + "\n";
                    }

                    if (buttonParams.Length % 2 == 1)
                        buttonParams = buttonParams + "\n";

                    //encrypted = new ButtonEncryption().SignAndEncrypt(
                    //    buttonParams, profile.CertificateFile, profile.PrivateKeyPassword, profile.PayPalCertificateFile);
                }
                catch (Exception e)
                {
                    throw new TransactionException(e.Message, e);
                }
                //CODECHNG:EWP_42.0_11613_Dhanesh_Start
                // Write the encrypted data to the output file
                if ((encrypted != null) && (encrypted.Length > 0))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<form action=\"");
                    //gets redirect URL from the Profile to the Html Pay Button.
                    sb.Append(profile.Url);
                    sb.Append("/cgi-bin/webscr\" method=\"post\">");
                    sb.Append("<table cellpadding=0 cellspacing=0  border=1><tr><td align=left><font face=Verdana color=black size=2>Item:</font></td><td align=left><font face=Verdana color=black size=2>Price:</font></td><td align=left>&nbsp;</td></tr><tr><td align=left><font face=Verdana color=black size=2>");
                    sb.Append(itemName);
                    sb.Append("</font></td><td align=left><font face=Verdana color=black size=2>");
                    sb.Append(itemAmount);
                    sb.Append("</font></td><td align=left>");
                    sb.Append("<input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">"); ;
                    sb.Append("<input type=\"image\" src=\"");
                    //gets Button Image from the Profile.
                    sb.Append(profile.ButtonImage);
                    sb.Append("\" border=\"0\" name=\"submit\" ");
                    sb.Append("alt=\"Make payments with PayPal - it's fast, free and secure!\">");
                    sb.Append("<input type=\"hidden\" name=\"encrypted\" value=\"");
                    sb.Append(encrypted);
                    sb.Append("\">");
                    sb.Append("</td></tr></table>");
                    sb.Append("</form>");
                    return sb.ToString();
                }
                else
                {
                    throw new TransactionException(MessageResources.GetMessage("ENCRYPTION_ERROR"));
                }
                //CODECHNG:EWP_42.0_11613_Dhanesh_End
            }*/
        #endregion
    } 
}