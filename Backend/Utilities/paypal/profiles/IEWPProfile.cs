/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using com.paypal.wps.rules;

namespace teamdk.Backend.Utilities.paypal.profiles
{
	/// <summary>
	/// Public interface to the properties that encapsulate details of an entity that utilizes the PayPal SDK.
	/// </summary>
	public interface IEWPProfile
	{
		/// <summary>
		/// The certificate used to encrypt button code
		/// </summary>
		string CertificateFile{get; set;}


		/// <summary>
		/// Path to merchant private key, used to sign encrypted data
		/// </summary>
		string PrivateKey{get; set;}


		/// <summary>
		/// The private key password
		/// </summary>
		string PrivateKeyPassword{get; set;}


		/// <summary>
		/// The PayPal certificate used to encrypt button code
		/// </summary>
		string PayPalCertificateFile{get; set;}


		/// <summary>
		/// URL that buttons should post form data to, eg. www.paypal.com
		/// </summary>
		string Url{get; set;}


		/// <summary>
		/// URL of graphic to use for button image
		/// </summary>
		string ButtonImage{get; set;}


		/// <summary>
		/// Ensures that essential Profile values have been defined. This is meant for information purposes, hence if a problem is found no exception is thrown. Instead, one or more error messages are returned, and the user can take appropriate action.
		/// </summary>
		/// <returns>collection of errors found</returns>
		ValidationErrors Validate();
	} // IEWPProfile Interface
} // profiles nameapce
