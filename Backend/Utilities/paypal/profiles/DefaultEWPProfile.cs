/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System;
using System.IO;
using teamdk.Backend.Utilities.paypal.profiles;
using com.paypal.wps.rules;
using teamdk.Backend.Utilities.paypal.util;

namespace teamdk.Backend.Utilities.paypal.profiles
{
	/// <summary>
	/// Encrypted Website Payments profile
	/// </summary>
	[Serializable]
	public class DefaultEWPProfile : IEWPProfile
	{
		private string certificateFile;

		[NonSerialized] private string privateKeyPassword;

		private string payPalCertificateFile;

		private string privateKey;

		private string url;

		private string buttonImage="https://www.paypal.com/en_US/i/btn/x-click-but23.gif";


		/// <summary>
		/// The certificate file path
		/// </summary>
		public string CertificateFile
		{
			get
			{
				return this.certificateFile;
			}
			set
			{
				this.certificateFile = value;
			}
		}


		/// <summary>
		/// The private key password
		/// </summary>
		public string PrivateKeyPassword
		{
			get
			{
				return this.privateKeyPassword;
			}
			set
			{
				this.privateKeyPassword = value;
			}
		}


		/// <summary>
		/// The PayPal certificate file path
		/// </summary>
		public string PayPalCertificateFile
		{
			get
			{
				return this.payPalCertificateFile;
			}
			set
			{
				this.payPalCertificateFile = value;
			}
		}


		/// <summary>
		/// The path to the private key
		/// </summary>
		public string PrivateKey
		{
			get
			{
				return this.privateKey;
			}
			set
			{
				this.privateKey = value;
			}
		}


		/// <summary>
		/// The endpoint URL, eg. "https://www.paypal.com"
		/// </summary>
		public string Url
		{
			get
			{
				return this.url;
			}
			set
			{
				this.url = value;
			}
		}


		/// <summary>
		/// The URL of button image
		/// </summary>
		public string ButtonImage
		{
			get
			{
				return this.buttonImage;
			}
			set
			{
				this.buttonImage = value;
			}
		}


		/// <summary>
		/// Validate the profile, make sure all values are non-null
		/// </summary>
		/// <returns>True if and only if all values are non-null</returns>
		public ValidationErrors Validate() 
		{
			ValidationErrors errors = new ValidationErrors();
			if ((this.certificateFile == null) || (this.certificateFile.Length < 1)) 
			{
				errors.Add(new ValidationError(MessageResources.GetMessage("EWP_CERTIFICATE_FILE_BLANK")));
			}
			else
			{
				if (!File.Exists(this.CertificateFile))
				{
					errors.Add(new ValidationError(MessageResources.GetMessage("EWP_CERTIFICATE_FILE_MISSING")));
				}
			}
			if ((this.payPalCertificateFile == null) || (this.payPalCertificateFile.Length < 1)) 
			{
				errors.Add(new ValidationError(MessageResources.GetMessage("EWP_PAYPAL_CERTIFICATE_BLANK")));
			}
			else
			{
				if (!File.Exists(this.PayPalCertificateFile))
				{
					errors.Add(new ValidationError(MessageResources.GetMessage("EWP_PAYPAL_CERTIFICATE_MISSING")));
				}
			}
			if ((this.privateKey == null) || (this.privateKey.Length < 1)) 
			{
				errors.Add(new ValidationError(MessageResources.GetMessage("EWP_PRIVATE_KEY_BLANK")));
			}
			else
			{
				if (!File.Exists(this.PrivateKey))
				{
					errors.Add(new ValidationError(MessageResources.GetMessage("EWP_PRIVATE_KEY_MISSING")));
				}
			}
			if ((this.url == null) || (this.url.Length < 1)) 
			{
				errors.Add(new ValidationError(MessageResources.GetMessage("EWP_URL_BLANK")));
			}
			if ((this.buttonImage == null) || (this.buttonImage.Length < 1)) 
			{
				errors.Add(new ValidationError(MessageResources.GetMessage("EWP_IMAGE_BLANK")));
			}
			return errors;
		} // Validate
	} // DefaultEWPProfile
} // profiles namespace