/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using teamdk.Backend.Utilities.paypal.profiles;

namespace teamdk.Backend.Utilities.paypal.profiles
{
	/// <summary>
	/// Factory class which makes new profile objects
	/// </summary>
	public class ProfileFactory 
	{

		/// <summary>
		/// Create an EWP profile object
		/// </summary>
		/// <returns>An EWP Profile</returns>
		public static IEWPProfile CreateEWPProfile() 
		{
			return new DefaultEWPProfile();
		}
	} // ProfileFactory
} // profiles namespace