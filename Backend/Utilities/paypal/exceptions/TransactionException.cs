/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System;
using teamdk.Backend;


namespace com.paypal.wps.exceptions
{
	/// <summary>
	/// Exception that occurs within the context of a transaction 
	/// </summary>
	public class TransactionException : PayPalException
	{

		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		public TransactionException() : base()
		{}


		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		public TransactionException(string message): base(message)
		{
			Minilogger.Debug(message);
		}


		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		/// <param name="cause">The exception that is the cause of the current exception</param>
		public TransactionException(string message, Exception cause): base(message, cause)
		{
            Minilogger.Debug(message);

		}
	} // TransactionException
} // exceptions namespace