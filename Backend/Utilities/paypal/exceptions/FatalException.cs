/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System;


using teamdk.Backend;

namespace com.paypal.wps.exceptions
{
	/// <summary>
	/// Exception that causes the SDK to cease functioning properly
	/// </summary>
	public class FatalException : PayPalException
	{
		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		public FatalException() : base() {}


		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		public FatalException(string message) : base(message)
		{
		    Minilogger.Debug(message);
		}


		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		/// <param name="cause">The exception that is the cause of the current exception</param>
		public FatalException(string message, Exception cause) : base(message, cause)
		{
            Minilogger.Debug(message);

		}
	} // FatalException
} // exceptions namespace