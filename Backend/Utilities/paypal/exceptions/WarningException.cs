/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System;
using teamdk.Backend;


namespace com.paypal.wps.exceptions
{
	/// <summary>
	/// SDK Warning
	/// </summary>
	public class WarningException:PayPalException
	{
		
		/// <summary>
		/// SDK Warning
		/// </summary>
		public WarningException() : base() {}


		/// <summary>
		/// SDK Warning
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		public WarningException(string message): base(message)
		{
		    Minilogger.Debug(message);
		}


		/// <summary>
		/// SDK Warning
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		/// <param name="cause">The exception that is the cause of the current exception</param>
		public WarningException(string message, Exception cause): base(message, cause)
		{
		    Minilogger.Debug(message);
		}
	} // WarningException class
} // exceptions namespace