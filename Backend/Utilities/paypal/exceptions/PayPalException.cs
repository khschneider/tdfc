/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System;

namespace com.paypal.wps.exceptions
{
	/// <summary>
	/// Superclass for all PayPal exception classes
	/// </summary>
	public abstract class PayPalException : Exception
	{
		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		public PayPalException() : base() {}


		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		public PayPalException(string message): base(message) {}


		/// <summary>
		/// Represents errors that occur during application execution
		/// </summary>
		/// <param name="message">The message that describes the error</param>
		/// <param name="cause">The exception that is the cause of the current exception</param>
		public PayPalException(string message, Exception cause): base(message, cause) {}
	} // PayPalException
} // exceptions namespace