/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System;
using System.Collections;

namespace com.paypal.wps.rules
{
	/// <summary>
	/// Contains a collection of errors regarding things that was wrong when a profile was validated.
	/// </summary>
	[Serializable]
	public class ValidationErrors
	{
		private bool accessed = false;
		private ArrayList errors = new ArrayList();


		/// <summary>
		/// Add an error to the collection
		/// </summary>
		/// <param name="error">error to add</param>
		public void Add(ValidationError error)
		{
			errors.Add(error);
		}


		/// <summary>
		/// Clear all errors from the object
		/// </summary>
		public void Clear()
		{
			errors.Clear();
		}


		/// <summary>
		/// Verify if this object contains any errors
		/// </summary>
		/// <returns>True if and only if there are no errors in this object</returns>
		public bool IsEmpty()
		{
			if (errors.Count == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// Get an enumerator over the collection of errors
		/// </summary>
		/// <returns>IEnumerator</returns>
		public System.Collections.IEnumerator GetEnumerator()
		{
			this.accessed = true;
			return errors.GetEnumerator();		
		}

		/// <summary>
		/// Determine whether or not get() has been called
		/// </summary>
		/// <returns>true if get() has been called</returns>
		public bool IsAccessed()
		{
			return this.accessed;
		}


		/// <summary>
		/// Return the number of errors in this object
		/// </summary>
		/// <returns>number of errors</returns>
		public int Size()
		{
			return errors.Count;
		} // Size()
	} // ValidationErrors class
} // rules namespace