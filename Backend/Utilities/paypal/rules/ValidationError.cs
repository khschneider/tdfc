/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

using System;
using System.Collections;

namespace com.paypal.wps.rules
{
	/// <summary>
	/// Contains information regarding something that was wrong when a profile was validated.
	/// </summary>
	[Serializable]
	public class ValidationError
	{
		private string key;


		/// <summary>
		/// The error message
		/// </summary>
		public string Key 
		{
			get 
			{
				return this.key;
			}
			set
			{
				this.key = value;
			}
		} // Key property


		/// <summary>
		/// Constructor provides the error message
		/// </summary>
		/// <param name="key"></param>
		public ValidationError(string key)
		{
			this.key = key;
		} // constructor
	} // ValidationError class
} // rules namespace