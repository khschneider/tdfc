﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MailChimp;
using MailChimp.Types;
using teamdk.Backend.DomainModel;
using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.web;

namespace teamdk.Backend.Utilities
{
    public class MailChimpApiWrapper
    {
        private static Settings settings;
        private static MCApi mailChimpCommunicator;

        public MailChimpApiWrapper()
        {
            if(settings==null)
            {
                settings = TeamDkDataContext.Instance.Settingss.Single();
            }
            if (mailChimpCommunicator == null)
            {
                mailChimpCommunicator = new MCApi("4250b43a834628a3651d59c7b8f9226b-us6", true);
            }
        }
        public  bool SubscribeToList(AktiviteterEnum type, Medlem medlem, string userHostAddress)
        {
           var subscribeOptions =
            new Opt<List.SubscribeOptions>(
                new List.SubscribeOptions
                {
                    SendWelcome = false,
                    DoubleOptIn = false
                });

            var merges =
                new Opt<List.Merges>(
                    new List.Merges
					{
						{"FNAME", medlem.FirstName}
					});

            var response = false;
            switch (type)
            {
                case AktiviteterEnum.Alle:
                    response = mailChimpCommunicator.ListSubscribe(settings.ListIdAlle,medlem.email,merges,subscribeOptions);
                    break;
                case AktiviteterEnum.Løb:
                    response = mailChimpCommunicator.ListSubscribe(settings.ListIdLb, medlem.email, merges, subscribeOptions);
                    break;
                case AktiviteterEnum.Fodbold:
                    response = mailChimpCommunicator.ListSubscribe(settings.ListIdFodbold, medlem.email, merges, subscribeOptions);
                    break;
                case AktiviteterEnum.Golf:
                    response = mailChimpCommunicator.ListSubscribe(settings.ListIdGolf, medlem.email, merges, subscribeOptions);
                    break;
                case AktiviteterEnum.Social:
                    response = mailChimpCommunicator.ListSubscribe(settings.ListIdSocial, medlem.email, merges, subscribeOptions);
                    break;
            }
            return response;
        }

        public bool UnSubscribeToList(AktiviteterEnum type, Medlem medlem, string userHostAddress)
        {
            var unSubscribeOptions =
            new Opt<List.UnsubscribeOptions>(
                new List.UnsubscribeOptions
                {
                   DeleteMember = true,
                   SendGoodby = true,
                   SendNotify = false
                });
            bool response = false;
            switch (type)
            {
                case AktiviteterEnum.Alle:
                    response = mailChimpCommunicator.ListUnsubscribe(settings.ListIdAlle, medlem.email, unSubscribeOptions);
                    break;
                case AktiviteterEnum.Løb:
                    response = mailChimpCommunicator.ListUnsubscribe(settings.ListIdLb, medlem.email, unSubscribeOptions);
                    break;
                case AktiviteterEnum.Fodbold:
                    response = mailChimpCommunicator.ListUnsubscribe(settings.ListIdFodbold, medlem.email, unSubscribeOptions);
                    break;
                case AktiviteterEnum.Golf:
                    response = mailChimpCommunicator.ListUnsubscribe(settings.ListIdGolf, medlem.email, unSubscribeOptions);
                    break;
                case AktiviteterEnum.Social:
                    response = mailChimpCommunicator.ListUnsubscribe(settings.ListIdSocial, medlem.email, unSubscribeOptions);
                    break;
            }
            return response;
        }

        //public void SendEventCreation(Event sender)
        //{
        //    var l = mailChimpCommunicator.ListMembers(settings.ListIdLb, List.MemberStatus.Subscribed).Data;
            
        //    var msg = new Mandrill.Messages.Message();
        //    msg.FromEmail = "info@teamdenmarkfc.dk";
        //    var toList = new List<MailChimp.Types.Mandrill.Messages.Recipient>();
        //    foreach (var member in l)
        //    {
        //        toList.Add(new Mandrill.Messages.Recipient(member.Email,member.Description));
        //    }
        //    msg.To = toList.ToArray();
        //    msg.Subject = "Nyt fra løbeklubben";
        //    msg.Html = sender.Beskrivelse;
        //    mandrilCommunicator.Send(msg);
        //}
    }
}