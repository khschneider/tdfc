﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using teamdk.Backend.DomainModel;

namespace teamdk.Backend.Utilities
{
    public static class SessionWrapper
    {

        public static PaymentContainer CurrentPayment   
        {
            get
            {
                if (HttpContext.Current.Session["CurrentPayment"] == null)
                {
                    return null;
                }

                return (PaymentContainer)HttpContext.Current.Session["CurrentPayment"];
            }
            set
            {
                HttpContext.Current.Session["CurrentPayment"] = value;
            }
        }
    }
}