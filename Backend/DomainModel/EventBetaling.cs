﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace teamdk.Backend.DomainModel
{
    [Serializable()]
    public class EventBetaling
    {
        public bool Gratis { get; set; }
        public bool DiffBetaling { get; set; }

        public int DiffPris{ get; set; }
        public int Pris { get; set; }
        
        public String Serialize()
        {
            var outStream = new StringWriter();
            var s = new XmlSerializer(typeof(EventBetaling));
            s.Serialize(outStream, this);
            return outStream.ToString();
        }

        public static EventBetaling DeSerialize(string data)
        {
            var serializer = new XmlSerializer(typeof(EventBetaling));
            EventBetaling result;
            using (var reader = new StringReader(data))
            {
                result = (EventBetaling)serializer.Deserialize(reader);
            }
            return result;
        }
    }
}