﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;

namespace teamdk.Backend.DomainModel
{
    public class PaymentContainer
    {
        public bool isHalfQuota { get; set; }
        public bool isFullQuota { get; set; }
        public Event @event { get; set; }
        public bool isQuotaPayment { get; set; }
        public bool isItemPayment { get; set; }
        public Medlem medlem { get; set; }
        public double amount { get; set; }
        public string ReturnUrl { get; set; }
        private string buingItemName;
        public String ItemName
        {
            get
            {
                if (isItemPayment)
                {
                    return buingItemName;
                }
                else if (!this.isQuotaPayment)
                {
                    buingItemName = this.@event.NodeName; 
                }
                else
                {
                    buingItemName = "Kontingent";
                }
                return buingItemName;
            }
            set { buingItemName = value; }
        }


    }
}