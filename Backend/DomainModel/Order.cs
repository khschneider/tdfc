//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace teamdk.Backend.DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public int OrderId { get; set; }
        public bool isHalfQuota { get; set; }
        public bool isFullQuota { get; set; }
        public Nullable<int> eventId { get; set; }
        public bool isQuotaPayment { get; set; }
        public bool isItemPayment { get; set; }
        public int memberId { get; set; }
        public double amount { get; set; }
        public string ReturnUrl { get; set; }
        public string buingItemName { get; set; }
        public string ItemName { get; set; }
        public Nullable<System.Guid> Guid { get; set; }
        public string PaypalResponse { get; set; }
        public Nullable<System.DateTime> PayStart { get; set; }
        public Nullable<System.DateTime> PayEnd { get; set; }
    }
}
