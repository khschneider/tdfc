﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.member;
using umbraco.cms.businesslogic.web;

namespace teamdk.Backend.DomainModel.UmbracoObjectsDataContext
{
    public partial class Event
    {
        
        public EventBetaling StronglyTypedEventType
        {
            get
            {
                return EventBetaling.DeSerialize(this.EventType);
            }
        }

        public DateTime NextTidspunkt
        {
            get
            {
                return TidspunkterStronglyTyped.FirstOrDefault(t => t >= DateTime.Now);
            }
        }
        public IEnumerable<DateTime> TidspunkterStronglyTyped
        {
            get
            {

                if (string.IsNullOrEmpty(Tidspunkter)) return new[] {DateTime.MinValue};
                var times = this.Tidspunkter.Split(',').Select(tid => DateTime.Parse(tid));
                 return times.OrderBy(t => t);
            }
        }
        public void InsertDeltager(string memberId)
        {
            if (this.Deltagere != null && this.Deltagere.Contains(memberId)) return;
            var d = new Document(this.Id);
            d.getProperty("deltagere").Value = this.Deltagere + "," + memberId;
          //  d.Save();
            d.Publish(new User(0));
            umbraco.library.UpdateDocumentCache(d.Id);
        }

        public bool ErTilmeldt(int memId)
        {

            if (memId == 0) return false;
            if (string.IsNullOrEmpty(this.Deltagere)&&string.IsNullOrEmpty(this.BetalendeMedlemmer)) return false;
            try
            {
                if(Deltagere!=null && this.Deltagere.Contains(memId.ToString())) return true;
                if (BetalendeMedlemmer != null && this.BetalendeMedlemmer.Contains(memId.ToString())) return true;
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, ex.ToString());
                
                return false;
            }

            return false;
        }

        public void RemoveDeltager(string memberId)
        {
            var d = new Document(this.Id);
            d.getProperty("deltagere").Value = this.Deltagere.Replace("," + memberId, "");
            d.Save();
            d.Publish(new User(0));
            umbraco.library.UpdateDocumentCache(d.Id);
        }

        public static IEnumerable<Event> GetMemberEvents(int id)
        {
                
            var all =
            TeamDkDataContext.Instance.Eventss.Single();
            var list = new List<Event>();
            foreach (Event evt in all.EventChildren)
            {
                if (evt.Deltagere != null)
                {
                    if (evt.Deltagere.Contains(id.ToString()))
                    {
                        list.Add(evt);
                    }
                }
            if(evt.BetalendeMedlemmer!=null)
                {
                     if(evt.BetalendeMedlemmer.Contains(id.ToString()))
                    {
                        list.Add(evt);
                    }
                }
            }
            return list;
        }
    }
}