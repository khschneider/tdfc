﻿using System;
using umbraco.Linq.Core;

namespace teamdk.Backend.DomainModel.UmbracoObjectsDataContext
{
    public partial class TeamDkDataContext : UmbracoDataContext
    {
        #region Partials
        partial void OnCreated();
        #endregion

        public TeamDkDataContext()
            : base()
        {
            OnCreated();
        }

        public TeamDkDataContext(UmbracoDataProvider provider)
            : base(provider)
        {
            OnCreated();
        }


        public Tree<CarouselItem> CarouselItems
        {
            get
            {
                return this.LoadTree<CarouselItem>();
            }
        }
        public Tree<ContentPage> ContentPages
        {
            get
            {
                return this.LoadTree<ContentPage>();
            }
        }
        public Tree<DateFolder> DateFolders
        {
            get
            {
                return this.LoadTree<DateFolder>();
            }
        }
        public Tree<Event> Events
        {
            get
            {
                return this.LoadTree<Event>();
            }
        }
        public Tree<Events> Eventss
        {
            get
            {
                return this.LoadTree<Events>();
            }
        }
        public Tree<Gallery> Gallerys
        {
            get
            {
                return this.LoadTree<Gallery>();
            }
        }
        public Tree<Home> Homes
        {
            get
            {
                return this.LoadTree<Home>();
            }
        }
        public Tree<HomepageCarousel> HomepageCarousels
        {
            get
            {
                return this.LoadTree<HomepageCarousel>();
            }
        }
        public Tree<Settings> Settingss
        {
            get
            {
                return this.LoadTree<Settings>();
            }
        }
        public Tree<Nyhed> Nyheds
        {
            get
            {
                return this.LoadTree<Nyhed>();
            }
        }
        public Tree<Root> Roots
        {
            get
            {
                return this.LoadTree<Root>();
            }
        }
        public Tree<Section> Sections
        {
            get
            {
                return this.LoadTree<Section>();
            }
        }
        public Tree<TwoCollumnPage> TwoCollumnPages
        {
            get
            {
                return this.LoadTree<TwoCollumnPage>();
            }
        }
    }


    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("CarouselItem")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class CarouselItem : DocTypeBase
    {
        public CarouselItem()
        {
        }

        private String _Title;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("title", DisplayName = "Title", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Title
        {
            get
            {
                return this._Title;
            }
            set
            {
                if ((this._Title != value))
                {
                    this.RaisePropertyChanging();
                    this._Title = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Title");
                }
            }
        }
        private String _Content;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("content", DisplayName = "Content", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Content
        {
            get
            {
                return this._Content;
            }
            set
            {
                if ((this._Content != value))
                {
                    this.RaisePropertyChanging();
                    this._Content = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Content");
                }
            }
        }
        private Int32? _Image;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("image", DisplayName = "Image", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual Int32? Image
        {
            get
            {
                return this._Image;
            }
            set
            {
                if ((this._Image != value))
                {
                    this.RaisePropertyChanging();
                    this._Image = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Image");
                }
            }
        }
        private String _LinkTitle;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("linkTitle", DisplayName = "Link title", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String LinkTitle
        {
            get
            {
                return this._LinkTitle;
            }
            set
            {
                if ((this._LinkTitle != value))
                {
                    this.RaisePropertyChanging();
                    this._LinkTitle = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("LinkTitle");
                }
            }
        }
        private Int32? _ButtonImage;
        /// <summary>
        /// If a image is provided this will be rendered instead of the text
        /// </summary>
        [UmbracoInfo("buttonImage", DisplayName = "Button Image", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual Int32? ButtonImage
        {
            get
            {
                return this._ButtonImage;
            }
            set
            {
                if ((this._ButtonImage != value))
                {
                    this.RaisePropertyChanging();
                    this._ButtonImage = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("ButtonImage");
                }
            }
        }
        private Int32? _Link;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("link", DisplayName = "Link", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual Int32? Link
        {
            get
            {
                return this._Link;
            }
            set
            {
                if ((this._Link != value))
                {
                    this.RaisePropertyChanging();
                    this._Link = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Link");
                }
            }
        }


    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("ContentPage")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class ContentPage : Root
    {
        public ContentPage()
        {
        }

        private String _Header;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("header", DisplayName = "Overskrift", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Header
        {
            get
            {
                return this._Header;
            }
            set
            {
                if ((this._Header != value))
                {
                    this.RaisePropertyChanging();
                    this._Header = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Header");
                }
            }
        }
        private String _Content;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("content", DisplayName = "Indhold", Mandatory = true)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Content
        {
            get
            {
                return this._Content;
            }
            set
            {
                if ((this._Content != value))
                {
                    this.RaisePropertyChanging();
                    this._Content = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Content");
                }
            }
        }

        private AssociationTree<ContentPage> _ContentPages;
        public AssociationTree<ContentPage> ContentPages
        {
            get
            {
                if ((this._ContentPages == null))
                {
                    this._ContentPages = this.ChildrenOfType<ContentPage>();
                }
                return this._ContentPages;
            }
            set
            {
                this._ContentPages = value;
            }
        }
        private AssociationTree<Nyhed> _Nyheds;
        public AssociationTree<Nyhed> Nyheds
        {
            get
            {
                if ((this._Nyheds == null))
                {
                    this._Nyheds = this.ChildrenOfType<Nyhed>();
                }
                return this._Nyheds;
            }
            set
            {
                this._Nyheds = value;
            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("DateFolder")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class DateFolder : Section
    {
        public DateFolder()
        {
        }


        private AssociationTree<ContentPage> _ContentPages;
        public AssociationTree<ContentPage> ContentPages
        {
            get
            {
                if ((this._ContentPages == null))
                {
                    this._ContentPages = this.ChildrenOfType<ContentPage>();
                }
                return this._ContentPages;
            }
            set
            {
                this._ContentPages = value;
            }
        }
        private AssociationTree<DateFolder> _DateFolders;
        public AssociationTree<DateFolder> DateFolders
        {
            get
            {
                if ((this._DateFolders == null))
                {
                    this._DateFolders = this.ChildrenOfType<DateFolder>();
                }
                return this._DateFolders;
            }
            set
            {
                this._DateFolders = value;
            }
        }
        private AssociationTree<Event> _Events;
        public AssociationTree<Event> Events
        {
            get
            {
                if ((this._Events == null))
                {
                    this._Events = this.ChildrenOfType<Event>();
                }
                return this._Events;
            }
            set
            {
                this._Events = value;
            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Event")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Event : Root
    {
        public Event()
        {
        }

        private String _Aktivitet;
        /// <summary>
        /// Aktivitets typen bestemmer hvor og hvordan den bliver vist pÃ¥ siden.Samt mÃ¥lgruppen for mails.
        ///Hvis "Alle" er valgt bliver de andre typer ignoreret.
        /// </summary>
        [UmbracoInfo("aktivitet", DisplayName = "Aktivitet", Mandatory = true)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Aktivitet
        {
            get
            {
                return this._Aktivitet;
            }
            set
            {
                if ((this._Aktivitet != value))
                {
                    this.RaisePropertyChanging();
                    this._Aktivitet = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Aktivitet");
                }
            }
        }
        private String _Tidspunkter;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("tidspunkter", DisplayName = "Tidspunkter", Mandatory = true)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Tidspunkter
        {
            get
            {
                return this._Tidspunkter;
            }
            set
            {
                if ((this._Tidspunkter != value))
                {
                    this.RaisePropertyChanging();
                    this._Tidspunkter = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Tidspunkter");
                }
            }
        }
        private String _EventType;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("eventType", DisplayName = "Event type", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String EventType
        {
            get
            {
                return this._EventType;
            }
            set
            {
                if ((this._EventType != value))
                {
                    this.RaisePropertyChanging();
                    this._EventType = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("EventType");
                }
            }
        }
        private String _Beskrivelse;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("beskrivelse", DisplayName = "Beskrivelse", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Beskrivelse
        {
            get
            {
                return this._Beskrivelse;
            }
            set
            {
                if ((this._Beskrivelse != value))
                {
                    this.RaisePropertyChanging();
                    this._Beskrivelse = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Beskrivelse");
                }
            }
        }
        private String _Sted;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("sted", DisplayName = "Sted", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Sted
        {
            get
            {
                return this._Sted;
            }
            set
            {
                if ((this._Sted != value))
                {
                    this.RaisePropertyChanging();
                    this._Sted = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Sted");
                }
            }
        }
        private String _Deltagere;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("deltagere", DisplayName = "Deltagere", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Deltagere
        {
            get
            {
                return this._Deltagere;
            }
            set
            {
                if ((this._Deltagere != value))
                {
                    this.RaisePropertyChanging();
                    this._Deltagere = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Deltagere");
                }
            }
        }
        private String _BetalendeMedlemmer;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("betalendeMedlemmer", DisplayName = "Betalende Medlemmer", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String BetalendeMedlemmer
        {
            get
            {
                return this._BetalendeMedlemmer;
            }
            set
            {
                if ((this._BetalendeMedlemmer != value))
                {
                    this.RaisePropertyChanging();
                    this._BetalendeMedlemmer = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("BetalendeMedlemmer");
                }
            }
        }
        private String _EksterneDeltagere;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("eksterneDeltagere", DisplayName = "Eksterne deltagere", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String EksterneDeltagere
        {
            get
            {
                return this._EksterneDeltagere;
            }
            set
            {
                if ((this._EksterneDeltagere != value))
                {
                    this.RaisePropertyChanging();
                    this._EksterneDeltagere = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("EksterneDeltagere");
                }
            }
        }
        private String _EksterneBetalendeDeltagere;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("eksterneBetalendeDeltagere", DisplayName = "Eksterne betalende deltagere", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String EksterneBetalendeDeltagere
        {
            get
            {
                return this._EksterneBetalendeDeltagere;
            }
            set
            {
                if ((this._EksterneBetalendeDeltagere != value))
                {
                    this.RaisePropertyChanging();
                    this._EksterneBetalendeDeltagere = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("EksterneBetalendeDeltagere");
                }
            }
        }


    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Events")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Events : Section
    {
        public Events()
        {
        }


        private AssociationTree<DateFolder> _DateFolders;
        public AssociationTree<DateFolder> DateFolders
        {
            get
            {
                if ((this._DateFolders == null))
                {
                    this._DateFolders = this.ChildrenOfType<DateFolder>();
                }
                return this._DateFolders;
            }
            set
            {
                this._DateFolders = value;
            }
        }
        private AssociationTree<Event> _Events;
        public AssociationTree<Event> EventChildren
        {
            get
            {
                if ((this._Events == null))
                {
                    this._Events = this.ChildrenOfType<Event>();
                }
                return this._Events;
            }
            set
            {
                this._Events = value;
            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Gallery")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Gallery : Root
    {
        public Gallery()
        {
        }



    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Home")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Home : ContentPage
    {
        public Home()
        {
        }


        private AssociationTree<ContentPage> _ContentPages;
        public AssociationTree<ContentPage> ContentPages
        {
            get
            {
                if ((this._ContentPages == null))
                {
                    this._ContentPages = this.ChildrenOfType<ContentPage>();
                }
                return this._ContentPages;
            }
            set
            {
                this._ContentPages = value;
            }
        }
        private AssociationTree<Events> _Eventss;
        public AssociationTree<Events> Eventss
        {
            get
            {
                if ((this._Eventss == null))
                {
                    this._Eventss = this.ChildrenOfType<Events>();
                }
                return this._Eventss;
            }
            set
            {
                this._Eventss = value;
            }
        }
        private AssociationTree<Gallery> _Gallerys;
        public AssociationTree<Gallery> Gallerys
        {
            get
            {
                if ((this._Gallerys == null))
                {
                    this._Gallerys = this.ChildrenOfType<Gallery>();
                }
                return this._Gallerys;
            }
            set
            {
                this._Gallerys = value;
            }
        }
        private AssociationTree<Section> _Sections;
        public AssociationTree<Section> Sections
        {
            get
            {
                if ((this._Sections == null))
                {
                    this._Sections = this.ChildrenOfType<Section>();
                }
                return this._Sections;
            }
            set
            {
                this._Sections = value;
            }
        }
        private AssociationTree<TwoCollumnPage> _TwoCollumnPages;
        public AssociationTree<TwoCollumnPage> TwoCollumnPages
        {
            get
            {
                if ((this._TwoCollumnPages == null))
                {
                    this._TwoCollumnPages = this.ChildrenOfType<TwoCollumnPage>();
                }
                return this._TwoCollumnPages;
            }
            set
            {
                this._TwoCollumnPages = value;
            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("HomepageCarousel")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class HomepageCarousel : DocTypeBase
    {
        public HomepageCarousel()
        {
        }


        private AssociationTree<CarouselItem> _CarouselItems;
        public AssociationTree<CarouselItem> CarouselItems
        {
            get
            {
                if ((this._CarouselItems == null))
                {
                    this._CarouselItems = this.ChildrenOfType<CarouselItem>();
                }
                return this._CarouselItems;
            }
            set
            {
                this._CarouselItems = value;
            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Settings")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Settings : DocTypeBase
    {
        public Settings()
        {
        }

        private Int32? _HalfYearPrice;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("halfYearPrice", DisplayName = "Halv Ã¥rs medlemsskab pris", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual Int32? HalfYearPrice
        {
            get
            {
                return this._HalfYearPrice;
            }
            set
            {
                if ((this._HalfYearPrice != value))
                {
                    this.RaisePropertyChanging();
                    this._HalfYearPrice = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("HalfYearPrice");
                }
            }
        }
        private Int32? _FullYearPrice;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("fullYearPrice", DisplayName = "Halv Ã¥rs medlemsskab pris", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual Int32? FullYearPrice
        {
            get
            {
                return this._FullYearPrice;
            }
            set
            {
                if ((this._FullYearPrice != value))
                {
                    this.RaisePropertyChanging();
                    this._FullYearPrice = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("FullYearPrice");
                }
            }
        }
        private String _MailChimpApiKey;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("mailChimpApiKey", DisplayName = "Mail chimp api key", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String MailChimpApiKey
        {
            get
            {
                return this._MailChimpApiKey;
            }
            set
            {
                if ((this._MailChimpApiKey != value))
                {
                    this.RaisePropertyChanging();
                    this._MailChimpApiKey = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("MailChimpApiKey");
                }
            }
        }
        private String _ListIdAlle;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("listIdAlle", DisplayName = "list id alle", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String ListIdAlle
        {
            get
            {
                return this._ListIdAlle;
            }
            set
            {
                if ((this._ListIdAlle != value))
                {
                    this.RaisePropertyChanging();
                    this._ListIdAlle = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("ListIdAlle");
                }
            }
        }
        private String _ListIdLb;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("listIdLb", DisplayName = "list id lÃ¸b", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String ListIdLb
        {
            get
            {
                return this._ListIdLb;
            }
            set
            {
                if ((this._ListIdLb != value))
                {
                    this.RaisePropertyChanging();
                    this._ListIdLb = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("ListIdLb");
                }
            }
        }
        private String _ListIdFodbold;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("listIdFodbold", DisplayName = "list id fodbold", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String ListIdFodbold
        {
            get
            {
                return this._ListIdFodbold;
            }
            set
            {
                if ((this._ListIdFodbold != value))
                {
                    this.RaisePropertyChanging();
                    this._ListIdFodbold = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("ListIdFodbold");
                }
            }
        }
        private String _ListIdGolf;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("listIdGolf", DisplayName = "list id golf", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String ListIdGolf
        {
            get
            {
                return this._ListIdGolf;
            }
            set
            {
                if ((this._ListIdGolf != value))
                {
                    this.RaisePropertyChanging();
                    this._ListIdGolf = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("ListIdGolf");
                }
            }
        }
        private String _ListIdSocial;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("listIdSocial", DisplayName = "list id social", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String ListIdSocial
        {
            get
            {
                return this._ListIdSocial;
            }
            set
            {
                if ((this._ListIdSocial != value))
                {
                    this.RaisePropertyChanging();
                    this._ListIdSocial = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("ListIdSocial");
                }
            }
        }
        private String _MandrilApiKey;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("mandrilApiKey", DisplayName = "Mandril Api Key", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String MandrilApiKey
        {
            get
            {
                return this._MandrilApiKey;
            }
            set
            {
                if ((this._MandrilApiKey != value))
                {
                    this.RaisePropertyChanging();
                    this._MandrilApiKey = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("MandrilApiKey");
                }
            }
        }


    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Nyhed")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Nyhed : ContentPage
    {
        public Nyhed()
        {
        }

        private String _Intro;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("intro", DisplayName = "BrÃ¸dtekst", Mandatory = true)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual String Intro
        {
            get
            {
                return this._Intro;
            }
            set
            {
                if ((this._Intro != value))
                {
                    this.RaisePropertyChanging();
                    this._Intro = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("Intro");
                }
            }
        }
        private DateTime? _OrginalDato;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("orginalDato", DisplayName = "OrginalDato", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual DateTime? OrginalDato
        {
            get
            {
                return this._OrginalDato;
            }
            set
            {
                if ((this._OrginalDato != value))
                {
                    this.RaisePropertyChanging();
                    this._OrginalDato = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("OrginalDato");
                }
            }
        }


    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Root")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Root : DocTypeBase
    {
        public Root()
        {
        }

        private Int32? _UmbracoNaviHide;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("umbracoNaviHide", DisplayName = "hide in navigation", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual Int32? UmbracoNaviHide
        {
            get
            {
                return this._UmbracoNaviHide;
            }
            set
            {
                if ((this._UmbracoNaviHide != value))
                {
                    this.RaisePropertyChanging();
                    this._UmbracoNaviHide = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("UmbracoNaviHide");
                }
            }
        }
        private Int32? _UmbracoInternalRedirectId;
        /// <summary>
        /// 
        /// </summary>
        [UmbracoInfo("umbracoInternalRedirectId", DisplayName = "Redirect To", Mandatory = false)]
        [Property()]
        [System.Runtime.Serialization.DataMemberAttribute()]
        public virtual Int32? UmbracoInternalRedirectId
        {
            get
            {
                return this._UmbracoInternalRedirectId;
            }
            set
            {
                if ((this._UmbracoInternalRedirectId != value))
                {
                    this.RaisePropertyChanging();
                    this._UmbracoInternalRedirectId = value;
                    this.IsDirty = true;
                    this.RaisePropertyChanged("UmbracoInternalRedirectId");
                }
            }
        }

        private AssociationTree<Events> _Eventss;
        public AssociationTree<Events> Eventss
        {
            get
            {
                if ((this._Eventss == null))
                {
                    this._Eventss = this.ChildrenOfType<Events>();
                }
                return this._Eventss;
            }
            set
            {
                this._Eventss = value;
            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("Section")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class Section : Root
    {
        public Section()
        {
        }


        private AssociationTree<ContentPage> _ContentPages;
        public AssociationTree<ContentPage> ContentPages
        {
            get
            {
                if ((this._ContentPages == null))
                {
                    this._ContentPages = this.ChildrenOfType<ContentPage>();
                }
                return this._ContentPages;
            }
            set
            {
                this._ContentPages = value;
            }
        }
        private AssociationTree<Nyhed> _Nyheds;
        public AssociationTree<Nyhed> Nyheds
        {
            get
            {
                if ((this._Nyheds == null))
                {
                    this._Nyheds = this.ChildrenOfType<Nyhed>();
                }
                return this._Nyheds;
            }
            set
            {
                this._Nyheds = value;
            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    [UmbracoInfo("TwoCollumnPage")]
    [System.Runtime.Serialization.DataContractAttribute()]
    [DocType()]
    public partial class TwoCollumnPage : ContentPage
    {
        public TwoCollumnPage()
        {
        }


        private AssociationTree<ContentPage> _ContentPages;
        public AssociationTree<ContentPage> ContentPages
        {
            get
            {
                if ((this._ContentPages == null))
                {
                    this._ContentPages = this.ChildrenOfType<ContentPage>();
                }
                return this._ContentPages;
            }
            set
            {
                this._ContentPages = value;
            }
        }
        private AssociationTree<Nyhed> _Nyheds;
        public AssociationTree<Nyhed> Nyheds
        {
            get
            {
                if ((this._Nyheds == null))
                {
                    this._Nyheds = this.ChildrenOfType<Nyhed>();
                }
                return this._Nyheds;
            }
            set
            {
                this._Nyheds = value;
            }
        }
        private AssociationTree<TwoCollumnPage> _TwoCollumnPages;
        public AssociationTree<TwoCollumnPage> TwoCollumnPages
        {
            get
            {
                if ((this._TwoCollumnPages == null))
                {
                    this._TwoCollumnPages = this.ChildrenOfType<TwoCollumnPage>();
                }
                return this._TwoCollumnPages;
            }
            set
            {
                this._TwoCollumnPages = value;
            }
        }

    }  
}