﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace teamdk.Backend.DomainModel.UmbracoObjectsDataContext
{
    partial class TeamDkDataContext
    {
        /// <summary>
        /// Instance property which creates a new instance of the UmbracoNodesDataContext class
        /// if the instance has already been created within the current HttpContext then it returns the cached version.
        /// </summary>
        public static TeamDkDataContext Instance
        {
            get
            {
                try
                {
                    var context = HttpContext.Current.Items["UmbracoNodesDataContex"] as TeamDkDataContext;

                    if (context == null)
                    {
                        context = new TeamDkDataContext();
                        HttpContext.Current.Items["UmbracoNodesDataContext"] = context;
                    }

                    return context;
                }
                catch (Exception)
                {

                  var  context = new TeamDkDataContext();
                    HttpContext.Current.Items["UmbracoNodesDataContext"] = context;
                    return context;

                }
                
            }
        }
    }
}