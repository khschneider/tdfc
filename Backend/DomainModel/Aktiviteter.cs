﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uComponents.Core.DataTypes.EnumCheckBoxList;

namespace teamdk.Backend.DomainModel
{

    public enum AktiviteterEnum
    {
        [EnumCheckBoxList(Text = "Alle", Value = "Alle")]
        Alle,
        [EnumCheckBoxList(Text = "Fodbold", Value = "Fodbold")]
        Fodbold,
        [EnumCheckBoxList(Text = "Løb", Value = "Løb")]
        Løb,
        [EnumCheckBoxList(Text = "Golf", Value = "Golf")]
        Golf,
        [EnumCheckBoxList(Text = "Social", Value = "Social")]
        Social
    }
}