﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;
using teamdk.Backend.Utilities;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.member;

namespace teamdk.Backend.DomainModel
{
    public enum MedlemsTypeEnum
    {
        Passivt,
        Halvårs,
        Helårs
    }

    public class Medlem
    {
        private readonly Member _member;
        public Medlem(Member umbracoMember)
        {

            _member = umbracoMember;
        }

        public int Id
        {
            get
            {
                if (_member == null) throw new MemberNotInitializeException();
                return _member.Id;
            }
        }

        public String FullName
        {
            get
            {
                try
                {
                    return _member.LoginName;
                }
                catch (Exception)
                {
                    return "?";
                }
            }

        }

        public string FirstName
        {
            get { return (string)_member.getProperty("firstname").Value; }
            set { _member.getProperty("firstname").Value = value; }
        }

        public string LastName
        {
            get { return (string)_member.getProperty("lastname").Value; }
            set { _member.getProperty("lastname").Value = value; }
        }

        public String FacebookUserId
        {
            get { return (String)_member.getProperty("facebookUserId").Value; }
        }

        public DateTime BetalingsTidspunkt
        {
            get
            {
                if (_member.getProperty("betalingDato").Value == null) return DateTime.MinValue;
                if (_member.getProperty("betalingDato").Value.GetType().Name == "String")
                {
                    return DateTime.MinValue;
                }
                return (DateTime)_member.getProperty("betalingDato").Value;
            }
            set { _member.getProperty("betalingDato").Value = value; }

        }

        public DateTime UdløbsDato
        {
            get
            {
                if (MedlemsType == MedlemsTypeEnum.Halvårs)
                {
                    return BetalingsTidspunkt.AddMonths(6);
                }
                return BetalingsTidspunkt.AddMonths(12);

            }
        }

        public string MedlemsTypeNavn
        {
            get
            {
                var mt = MedlemsType;
                if (mt == MedlemsTypeEnum.Passivt) return "Ikke betalende medlem";
                return Enum.GetName(MedlemsType.GetType(),mt ) + " Medlem";
            }
        }

        public MedlemsTypeEnum MedlemsType
        {
            get
            {
                var roles = Roles.GetRolesForUser(_member.LoginName);
                if ((roles.Contains("Halvårs betaler")) && BetalingsTidspunkt.AddMonths(6) > DateTime.Now)
                    return MedlemsTypeEnum.Halvårs;
                if ((roles.Contains("Helårs betaler")) && BetalingsTidspunkt.AddMonths(12) > DateTime.Now)
                    return MedlemsTypeEnum.Helårs;
                Roles.RemoveUserFromRole(_member.LoginName, "Halvårs betaler");
                Roles.RemoveUserFromRole(_member.LoginName, "Helårs betaler");

                return MedlemsTypeEnum.Passivt;
            }
            set
            {
                switch (value)
                {
                    case MedlemsTypeEnum.Halvårs:
                        Roles.AddUserToRole(_member.LoginName, "Halvårs betaler");
                        break;
                    case MedlemsTypeEnum.Helårs:
                        Roles.AddUserToRole(_member.LoginName, "Helårs betaler");
                        break;
                }
            }
        }

        public List<AktiviteterEnum> NyhedsBrevTilmelding
        {
            get
            {
                var list = new List<AktiviteterEnum>();
                foreach (var aktiviteterEnum in ((string)_member.getProperty("nyhedsBrev").Value).Split(','))
                {
                    if (aktiviteterEnum == string.Empty) continue;
                    var val = (AktiviteterEnum)Enum.Parse(typeof(AktiviteterEnum), aktiviteterEnum);
                    list.Add(val);
                }
                return list;
            }
            set
            {
                var sb = new StringBuilder();
                foreach (var aktiviteterEnum in value)
                {
                    sb.Append(aktiviteterEnum + ",");
                }
                if (sb.Length == 0)
                {
                    _member.getProperty("nyhedsBrev").Value = string.Empty;
                }
                else
                {
                    _member.getProperty("nyhedsBrev").Value = sb.ToString(0, sb.Length - 1);
                }

            }
        }

        public string email
        {
            get { return _member.Email; }
        }

        public bool TilmeldNyhedsBrev(string aktivitet, string userIp)
        {
            try
            {
                var tilmeldinger = NyhedsBrevTilmelding;
                if (NyhedsBrevTilmelding.Any(a => a.ToString() == aktivitet)) return true;
                var type = (AktiviteterEnum)Enum.Parse(typeof(AktiviteterEnum), aktivitet);
                tilmeldinger.Add(type);
                NyhedsBrevTilmelding = tilmeldinger;
                var mw = new MailChimpApiWrapper();
                return mw.SubscribeToList(type, this, userIp);
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Error, 0, ex.ToString());
                return false;
            }
        }

        public bool FrameldNyhedsBrev(string aktivitet, string userIp)
        {
            var tilmeldinger = NyhedsBrevTilmelding;
            if (NyhedsBrevTilmelding.Count(a => a.ToString() == aktivitet) == 0) return true;
            var type = (AktiviteterEnum)Enum.Parse(typeof(AktiviteterEnum), aktivitet);
            tilmeldinger.Remove(type);
            NyhedsBrevTilmelding = tilmeldinger;
            var mw = new MailChimpApiWrapper();
            var res = mw.UnSubscribeToList(type, this, userIp);
            return res;
        }
    }

    public class MemberNotInitializeException : Exception
    {
    }
}