﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace teamdk.Backend
{
    public class Minilogger
    {
        private static readonly object SyncRoot = new object();

        public static void Debug(string txt)
        {
            lock (SyncRoot)
            {
                try
                {
                    var filePath = AppDomain.CurrentDomain.GetData("DataDirectory") + "/log.txt";
                    //string = Server.MapPath(strRequest); 
                    if (!System.IO.File.Exists(filePath))
                    {
                        System.IO.FileStream f = System.IO.File.Create(filePath);
                        f.Close();
                    }
                    using (System.IO.StreamWriter sw = System.IO.File.AppendText(filePath))
                    {
                        sw.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " :: " + txt);
                    }
                }

                catch (Exception rt)
                {
                    // Response.Write(rt.Message);
                }
            }
        }

        public static void Error(Exception exception)
        {
            Debug(exception.ToString());
        }

        public static void Error(string exception)
        {
            Debug(exception);
        }

        public static string GetLog()
        {
            var res = "";
            var path = AppDomain.CurrentDomain.GetData("DataDirectory") + "/log.txt";

            using (var sw = new System.IO.StreamReader(path))
            {
                res = sw.ReadToEnd();
            }
            return res;
        }
    }
}