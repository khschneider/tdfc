﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using umbraco.cms.businesslogic.property;

namespace teamdk.Backend
{
    public static class AzureWrapper
    {
        public static CloudStorageAccount storageAccount ;

        static AzureWrapper()
        {
            if(storageAccount==null)
            {
                storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["CloudConnectionString"]);
            }
            
        }
        public static string UploadToBlob(string path)
        {
            System.Threading.Thread.Sleep(100);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("tdfcmedia");
            var blob = container.GetBlobReference(path);
            blob.Properties.ContentType = Utilities.MIMEAssistant.GetMIMEType(path);
            using (var fileStream = System.IO.File.OpenRead(HttpRuntime.AppDomainAppPath+path))
            {
                blob.UploadFromStream(fileStream);
            }
            return "http://tdfc.blob.core.windows.net/tdfcmedia/" + path;
        }

        public static void DeleteBlob(string path)
        {
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("tdfcmedia");
            var blob = container.GetBlobReference(path);
            blob.DeleteIfExists();
        }
    }
}