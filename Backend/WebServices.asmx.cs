﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using teamdk.Backend.DomainModel;
using teamdk.Backend.DomainModel.UmbracoObjectsDataContext;
using umbraco.NodeFactory;
using umbraco.cms.businesslogic.member;

namespace teamdk.Backend
{
    /// <summary>
    /// Summary description for WebServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebServices : System.Web.Services.WebService
    {
   

        [WebMethod]
        public List<MedlemView> HentDeltagere(string eventId)
        {
            var id = int.Parse(eventId);
            var ctx = TeamDkDataContext.Instance;
            var @event = ctx.Events.Single(e => e.Id == id);
            var list = new List<MedlemView>();
            if (@event.Deltagere != null)
            {

                foreach (var delt in @event.Deltagere.Split(','))
                {
                    try
                    {

                        if (string.IsNullOrEmpty(delt)) continue;
                        var mem = new Medlem(new Member(int.Parse(delt)));
                        list.Add(new MedlemView(){FacebookUserId = mem.FacebookUserId,FullName = mem.FullName});
                    }
                    catch (Exception ex)
                    {
                       Minilogger.Error(ex);
                    }
                }
            }
            if (@event.BetalendeMedlemmer != null)
            {
                foreach (var betaler in @event.BetalendeMedlemmer.Split(','))
                {
                    try
                    {

                        if (string.IsNullOrEmpty(betaler)) continue;
                        var mem = new Medlem(new Member(int.Parse(betaler)));
                        list.Add(new MedlemView() { FacebookUserId = mem.FacebookUserId, FullName = mem.FullName });
                    }
                    catch (Exception ex)
                    {
                        Minilogger.Error(ex);
                    }
                }
            }
            var n = new Node(id);
            var prop = n.GetProperty("manueltTilfjet");
            if(prop!=null)
            {
                if(!string.IsNullOrEmpty(prop.Value))
                {
                    var xml = new XmlDocument();
                    xml.LoadXml(prop.Value);
                    var v= xml.GetElementsByTagName("values").Item(0);
                    var children = v.ChildNodes;
                    foreach (XmlNode child in children)
                    {
                        if (child.InnerText!="")
                        {
                            list.Add(new MedlemView() { FacebookUserId = "", FullName = child.InnerText });
                        }
                    }
                }
            }
            return list;
        }

        [WebMethod(EnableSession = true)]
        public void SetupPayment(string eventId, string memberId)
        {
            var id = int.Parse(eventId);
            var ctx = TeamDkDataContext.Instance;
            var @event = ctx.Events.Single(e => e.Id == id);
            var pc = new PaymentContainer();
            pc.ReturnUrl = this.Context.Request.UrlReferrer.LocalPath;
            pc.@event = @event;
            pc.medlem = new Medlem(new Member(int.Parse(memberId)));
            var amount = 0;
            if (@event.StronglyTypedEventType.DiffBetaling)
            {
                if (pc.medlem.MedlemsType == MedlemsTypeEnum.Passivt)
                {
                    amount = @event.StronglyTypedEventType.DiffPris;
                }
                else
                {
                    amount = @event.StronglyTypedEventType.Pris;
                }
            }
            else
            {
                amount = @event.StronglyTypedEventType.Pris;
            }
            pc.amount = amount;
            Utilities.SessionWrapper.CurrentPayment = pc;
        }

        [WebMethod(EnableSession = true)]
        public void SetupQuotaPayment(string quotaType, string memberId)
        {
            var pc = new PaymentContainer();
            pc.medlem = new Medlem(new Member(int.Parse(memberId)));
            var amount = 0;
            pc.isQuotaPayment = true;

            switch (quotaType)
            {
                case "half":
                    amount = TeamDkDataContext.Instance.Settingss.Single().HalfYearPrice.Value;
                    pc.isHalfQuota = true;
                    break;
                case "full":
                    amount = TeamDkDataContext.Instance.Settingss.Single().FullYearPrice.Value;
                    pc.isFullQuota = true;
                    break;
            }
            pc.amount = amount;
            pc.ReturnUrl = "/";
            Utilities.SessionWrapper.CurrentPayment = pc;
        }

        [WebMethod]
        public void Tilmeld(string eventId, string memberId)
        {
            var id = int.Parse(eventId);
            var ctx = TeamDkDataContext.Instance;
            var @event = ctx.Events.Single(e => e.Id == id);
            @event.InsertDeltager(memberId);
        }

        [WebMethod]
        public void Frameld(string eventId, string memberId)
        {
            var id = int.Parse(eventId);
            var ctx = TeamDkDataContext.Instance;
            var @event = ctx.Events.Single(e => e.Id == id);
            @event.RemoveDeltager(memberId);
        }

        [WebMethod]
        public bool OpdaterNyhedsBrev(bool check, string type, int memberId)
        {
            var mem = new Medlem(new Member(memberId));
            if (check)
            {
                return mem.TilmeldNyhedsBrev(type, HttpContext.Current.Request.UserHostAddress);
            }
            else
            {
                return mem.FrameldNyhedsBrev(type, HttpContext.Current.Request.UserHostAddress);
            }

        }

        [WebMethod]
        public List<Medlem> SendEmail(string eventId)
        {
            var id = int.Parse(eventId);
            var ctx = TeamDkDataContext.Instance;
            var @event = ctx.Events.Single(e => e.Id == id);
            var list = new List<Medlem>();
            if (@event.Deltagere == null) return list;

            foreach (var delt in @event.Deltagere.Split(','))
            {
                try
                {

                    if (string.IsNullOrEmpty(delt)) continue;
                    list.Add(new Medlem(new Member(int.Parse(delt))));
                }
                catch (Exception)
                {
                }
            }
            return list;
        }

        [WebMethod]
        public void SendEmail()
        {
            var mem = Member.GetCurrentMember();
            var medlem = new Medlem(mem);
            Email.SendOldMemberMail(medlem);
        }

      
    }
}
