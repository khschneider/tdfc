﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="teamdk.Azure" generation="1" functional="0" release="0" Id="622351bd-2dd5-40a2-a517-d3cc8c1e1bf9" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="teamdk.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="teamdk:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/teamdk.Azure/teamdk.AzureGroup/LB:teamdk:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="teamdk:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/teamdk.Azure/teamdk.AzureGroup/Mapteamdk:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="teamdk:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/teamdk.Azure/teamdk.AzureGroup/Mapteamdk:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="teamdkInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/teamdk.Azure/teamdk.AzureGroup/MapteamdkInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:teamdk:Endpoint1">
          <toPorts>
            <inPortMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdk/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="Mapteamdk:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdk/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="Mapteamdk:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdk/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapteamdkInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdkInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="teamdk" generation="1" functional="0" release="0" software="C:\Users\kms\Documents\Visual Studio 2010\Projects\teamdk\teamdk.Azure\csx\Debug\roles\teamdk" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;teamdk&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;teamdk&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdkInstances" />
            <sCSPolicyUpdateDomainMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdkUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdkFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="teamdkUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="teamdkFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="teamdkInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="2abf2374-8973-420f-b51c-9082f3613cba" ref="Microsoft.RedDog.Contract\ServiceContract\teamdk.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="59e9d1b5-3752-4420-84da-be2c0d5a52ca" ref="Microsoft.RedDog.Contract\Interface\teamdk:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/teamdk.Azure/teamdk.AzureGroup/teamdk:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>