﻿function HentDeltagere(eventId) {
    $.ajax({
        url: "/Backend/WebServices.asmx/HentDeltagere",
        data: "{ 'eventId': '" + eventId + "'}",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#deltagere ul').html("");
            $.each(data.d, function () {
                if(this.FacebookUserId.length==0) {
                    $('#deltagere ul').append("<li><img src='/imagegen.ashx/?image=/image/user.png&height=50'/>" + this.FullName + "</li>");
                } else {
                    $('#deltagere ul').append("<li><a href='http://www.facebook.com/people/@/" + this.FacebookUserId + "'><img src='https://graph.facebook.com/" + this.FacebookUserId + "/picture'/>" + this.FullName + "</a></li>");
                }
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}