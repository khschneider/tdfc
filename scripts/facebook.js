﻿function loginUser() {
    FB.login(
        function (response) {
            if (response.status === 'connected') {
                // the user is logged in and has authenticated your
                // app, and response.authResponse supplies
                // the user's ID, a valid access token, a signed
                // request, and the time the access token 
                // and signed request each expire
                var uid = response.authResponse.userID;
                var userAccessToken = response.authResponse.accessToken;


                $.post("/Base/FacebookLogin/LogUserIn", { accessToken: userAccessToken }, function callback(data) {
                    if (data != "error") {
                        parent.closeAndRefresh(); return false;
                    } else {
                        displayMessage("Error", "Oops! something has gone wrong we will look into it now.", "errormessage");
                        return "error";
                    }
                });

            } else if (response.status === 'not_authorized') {
                // the user is logged in to Facebook, 
                // but has not authenticated your app
            } else {
                // the user isn't logged in to Facebook.
            }
        },
    { scope: 'email, user_birthday' }
    );
    }

window.fbAsyncInit = function () {
    FB.init({
        appId: '403663173025186', // App ID
        channelUrl: 'teamdk.localhost', // Channel File
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true  // parse XFBML
    });

   
};

// Load the SDK Asynchronously
(function (d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
} (document));

function closeAndRefresh(parameters) {
    try {
        $.colorbox.close();  
    } catch(e) {

    } 
    location.reload();
}