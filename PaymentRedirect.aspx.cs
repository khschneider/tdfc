﻿using System;
using System.Web;
using teamdk.Backend.DomainModel;
using teamdk.Backend.Utilities.paypal.util;
using teamdk.Backend.Utilities.paypal.util;

namespace teamdk.Backend.Utilities
{
    public partial class PaymentRedirect : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                var domain = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                var currentPayment = SessionWrapper.CurrentPayment;
                var order = new Order
                {
                    Guid = Guid.NewGuid(),
                    ItemName = currentPayment.ItemName,
                    amount = currentPayment.amount,
                    buingItemName = currentPayment.ItemName,
                    isFullQuota = currentPayment.isFullQuota,
                    isHalfQuota = currentPayment.isHalfQuota,
                    isItemPayment = currentPayment.isItemPayment,
                    isQuotaPayment = currentPayment.isQuotaPayment,
                };
                order.PayStart = DateTime.Now;
                order.ReturnUrl = "";
                
                if (currentPayment.ReturnUrl != null)
                {
                    order.ReturnUrl = currentPayment.ReturnUrl;
                }

                if (System.Diagnostics.Debugger.IsAttached)
                {
                    order.memberId = 4448;
                }
                else
                {
                    order.memberId = currentPayment.medlem.Id;
                }

                if (currentPayment.@event != null)
                {
                    order.eventId = currentPayment.@event.Id;
                }
                string paypalCertPath = Server.MapPath("/certs/paypal_cert_pem.txt");//paypal_cert_pem
                string signerPfxPath = Server.MapPath("/certs/tdkfc_pkcs12.p12");//tdkfc_pkcs12
                string signerPfxPassword = "TDFC2004";//TDFC2004
                var firstName = "Paypal ";
                var lastname = "via købknap";
                if (currentPayment.medlem != null)
                {
                    firstName = currentPayment.medlem.FirstName;
                    lastname = currentPayment.medlem.LastName;
                }
                
                    string clearText =
                    "cmd=_xclick\n"
                    + "business=teamdenmarkfc@gmail.com\n" 
                    + "first_name=" + firstName + "\n"
                    + "last_name=" + lastname + "\n"
                    + "country=uk\n"
                    + "currency_code=GBP\n"
                    + "item_name=" + currentPayment.ItemName + "\n"
                    + "amount=" + currentPayment.amount.ToString().Replace(',','.') + "\n"
                    + "no_shipping=2\n"
                    + "no_note=1\n"
                    + "custom=" + order.Guid + "\n"
                    + "return=http://" + domain + "/paymentreceipt\n"
                    + "cancel_return=http://" + domain + "/paymentreceipt?cancel=true\n"
                    //+ "notify_url=http://"+domain+"/backend/webservice/InstantPaymentNotificationHandler.ashx?session=" + Session.SessionID + "\n"
                    + "rm=0\n"
                    + "cbt=Tilbage igen\n"
                    + "cert_id=KXHRVHQCEFVJS";// https://www.paypal.com/cgi-bin/customerprofileweb?cmd=_profile-website-cert

                var ewp = new ButtonEncryption();
                ewp.LoadSignerCredential(signerPfxPath, signerPfxPassword);
                ewp.RecipientPublicCertPath = paypalCertPath;
                string result = ewp.SignAndEncrypt(clearText);
                btnPayPal.PostBackUrl = "https://www.paypal.com/cgi-bin/webscr";
                strEncrypt.Text = "<input type=\"hidden\" name=\"encrypted\" value=\"" + result + " \" />";

                using (var ctx = new Entities())
                {
                    ctx.Orders.Add(order);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                strEncrypt.Text = "Somthing went wrong. :-( " + ex.ToString();

            }
        }
    }
}